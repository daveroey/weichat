package com.dave.weixin.configuation;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Created by Dave on 2018/7/7
 * Describes
 */
@Configuration
public class WeiChatConfiguation {


    @Bean
    public RestTemplate restTemplate() {
        StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        return new RestTemplateBuilder().additionalMessageConverters(converter).build();
    }

}
