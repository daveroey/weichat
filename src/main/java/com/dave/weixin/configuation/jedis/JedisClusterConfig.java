package com.dave.weixin.configuation.jedis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Dave on 2018/7/8
 * Describes
 */
@Configuration
public class JedisClusterConfig {
    @Autowired
    private RedisProperties redisProperties;


    @Bean
    public JedisCluster getJedisCluster() {
        //获取服务器数组(这里要相信自己的输入,忽略空指针。。)
        String[] serverArray = redisProperties.getNodes().split(",");
        Set<HostAndPort> nodes = new HashSet<>();
        for (String ipPort : serverArray) {
            String[] ipPortPair = ipPort.split(":");
            nodes.add(new HostAndPort(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())));
        }
        new RedisTemplate<>();
        //免密构造
        return new JedisCluster(nodes, redisProperties.getCommandTimeout(), 1000, 1, new GenericObjectPoolConfig());
    }
}
