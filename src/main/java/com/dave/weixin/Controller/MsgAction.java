package com.dave.weixin.Controller;

import com.dave.weixin.common.tulin.util.GetMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Dave on 2018/7/7
 * Describes
 */
@RestController
@RequestMapping(value = "/msg/*")
public class MsgAction {
    @Autowired
    private GetMessage getMessage;
    @Autowired
    JedisCluster cluster;

    /**
     * 和图灵机器人聊天
     *
     * @param msg
     * @return
     * @throws JsonProcessingException
     */
    @ResponseBody
    @RequestMapping(value = "/getMsg", method = RequestMethod.POST)
    public String getMsg(@RequestBody Map<String, String> msg) throws IOException {
        return getMessage.getResponseText(msg.get("text"));
    }

    /**
     * 和图灵机器人斗图
     *
     * @param msg
     * @return
     * @throws JsonProcessingException
     */
    @ResponseBody
    @RequestMapping(value = "/getImage", method = RequestMethod.POST)
    public String getImage(@RequestBody Map<String, String> msg) throws IOException {
        return getMessage.getResponseImage(msg.get("url"));
    }

    /**
     * 和图灵机器人语音骚聊
     *
     * @param msg
     * @return
     * @throws JsonProcessingException
     */
    @ResponseBody
    @RequestMapping(value = "/getMedia", method = RequestMethod.POST)
    public String getMedia(@RequestBody Map<String, String> msg) throws IOException {
        return getMessage.getResponseMedia(msg.get("url"));
    }
}
