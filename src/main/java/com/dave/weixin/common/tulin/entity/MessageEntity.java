package com.dave.weixin.common.tulin.entity;

import com.dave.weixin.common.tulin.constants.TulinConstants;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dave on 2018/7/7
 * Describes
 */
public class MessageEntity implements Serializable {

    private Map<String, Object> entity = new HashMap<>();
    //默认请求为文本
    private String reqType = "0";

    //用户信息
    private Map<String, Object> perception = new HashMap<>();
    private Map<String, Object> selfInfo = new HashMap<>();
    private Map<String, Object> location = new HashMap<>();
    private String city = TulinConstants.CITY;
    private String province = TulinConstants.PROVINCE;
    private String street = TulinConstants.STREET;

    //消息类型
    private Map<String, Object> inputText = new HashMap<>();
    private Map<String, Object> inputImage = new HashMap<>();
    private Map<String, Object> inputMedia = new HashMap<>();

    //账户信息
    private Map<String, Object> userInfo = new HashMap<>();
    private String apiKey = TulinConstants.API_KEY;
    private String userId = TulinConstants.USER_ID;


    /**
     * 组装信息
     *
     * @return
     */
    public Map<String, Object> getEntity() {
        this.entity.put("reqType", getReqType());
        this.entity.put("perception", getPerception());
        this.entity.put("userInfo", getUserInfo());
        return entity;
    }


    public String getReqType() {
        return reqType;
    }

    private void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public Map<String, Object> getPerception() {
        this.perception.put("inputText", getInputText());
        this.perception.put("inputImage", getInputImage());
        this.perception.put("inputMedia", getInputMedia());
        this.perception.put("selfInfo", getSelfInfo());
        return perception;
    }


    public Map<String, Object> getUserInfo() {
        this.userInfo.put("apiKey", getApiKey());
        this.userInfo.put("userId", getUserId());
        return userInfo;
    }

    public Map<String, Object> getInputMedia() {
        if (inputMedia.isEmpty()) {
            inputMedia.put("url", "");
        }
        return inputMedia;
    }


    public void setInputMedia(String url) {
        //音频
        setReqType("2");
        this.inputMedia.put("url", url);
    }


    public Map<String, Object> getInputText() {
        if (inputText.isEmpty()) {
            inputText.put("text", "");
        }
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText.put("text", inputText);
    }

    public Map<String, Object> getInputImage() {
        if (inputImage.isEmpty()) {
            inputImage.put("url", "");
        }
        return inputImage;
    }

    public void setInputImage(String url) {
        //图片类型
        setReqType("1");
        this.inputImage.put("url", url);
    }

    public Map<String, Object> getLocation() {
        this.location.put("city", getCity());
        this.location.put("province", getProvince());
        this.location.put("street", getStreet());
        return location;
    }

    public Map<String, Object> getSelfInfo() {
        this.selfInfo.put("location", getLocation());
        return selfInfo;
    }

    public String getCity() {
        return city;
    }


    public String getProvince() {
        return province;
    }


    public String getStreet() {
        return street;
    }


    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
