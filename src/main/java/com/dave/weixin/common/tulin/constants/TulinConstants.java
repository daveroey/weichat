package com.dave.weixin.common.tulin.constants;

import java.util.ResourceBundle;

/**
 * Created by Dave on 2018/7/8
 * Describes
 */
public class TulinConstants {
    //账号信息
    public final static String CITY = ResourceBundle.getBundle("tulin").getString("city");
    public final static String PROVINCE = ResourceBundle.getBundle("tulin").getString("province");
    public final static String STREET = ResourceBundle.getBundle("tulin").getString("street");
    public final static String API_KEY = ResourceBundle.getBundle("tulin").getString("apiKey");
    public final static String USER_ID = ResourceBundle.getBundle("tulin").getString("userId");


}
