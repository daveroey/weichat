package com.dave.weixin.common.activiti.service;

import org.springframework.stereotype.Service;

/**
 * @Author Dave
 * @Date 2018/7/26
 * @Description
 */
@Service
public class ResumeService {

    public void storeResume() {
        System.out.println("Storing resume ...");
    }
}
