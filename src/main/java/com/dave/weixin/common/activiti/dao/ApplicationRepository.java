package com.dave.weixin.common.activiti.dao;

import com.dave.weixin.common.activiti.entity.Application;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Dave
 * @Date 2018/7/26
 * @Description
 */
public interface ApplicationRepository extends JpaRepository<Application, Long> {

}
