package com.dave.weixin.common.activiti.dao;

import com.dave.weixin.common.activiti.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author Dave
 * @Date 2018/7/25
 * @Description
 */
public interface PersonRepository extends JpaRepository<Person, Long> {
    /**
     * @param username
     * @return
     */
    Person findByUsername(String username);


}
