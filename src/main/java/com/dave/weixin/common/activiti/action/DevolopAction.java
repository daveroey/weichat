package com.dave.weixin.common.activiti.action;

import com.dave.weixin.common.activiti.dao.ApplicationRepository;
import com.dave.weixin.common.activiti.entity.Application;
import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Dave
 * @Date 2018/7/26
 * @Description
 */
@RestController
public class DevolopAction {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ApplicationRepository applicantRepository;

    @RequestMapping(value = "/start-hire-process", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void startHireProcess(@RequestBody Application application) {

        applicantRepository.save(application);
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("applicant", application);
        runtimeService.startProcessInstanceByKey("hireProcess", variables);
    }
}
