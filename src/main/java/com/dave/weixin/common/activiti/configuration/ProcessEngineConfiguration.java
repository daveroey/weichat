package com.dave.weixin.common.activiti.configuration;

import org.springframework.context.annotation.Configuration;

/**
 * @Author Dave
 * @Date 2018/7/25
 * @Description
 */
@Configuration
public class ProcessEngineConfiguration {

    /*@Bean
    public CommandLineRunner init(final RepositoryService repositoryService,
                                  final RuntimeService runtimeService,
                                  final TaskService taskService) {

        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                System.out.println("Number of process definitions : "
                        + repositoryService.createProcessDefinitionQuery().count());
                System.out.println("Number of tasks : " + taskService.createTaskQuery().count());
                runtimeService.startProcessInstanceByKey("oneTaskProcess");
                System.out.println("Number of tasks after process start: " + taskService.createTaskQuery().count());
            }
        };

    }*/

    /*@Bean
    public CommandLineRunner init(final MyService myService) {

        return strings -> myService.createDemoUsers();

    }*/

    /*@Bean
    CommandLineRunner init(final RuntimeService runtimeService) {

        return strings -> {
            Map<String, Object> variables = new HashMap<>();
            variables.put("applicantName", "John Doe");
            variables.put("email", "john.doe@activiti.com");
            variables.put("phoneNumber", "123456789");
            runtimeService.startProcessInstanceByKey("hireProcess", variables);
        };
    }*/
}
