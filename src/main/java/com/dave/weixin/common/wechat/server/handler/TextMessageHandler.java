package com.dave.weixin.common.wechat.server.handler;

import com.dave.weixin.common.tulin.util.GetMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.foxinmy.weixin4j.handler.MessageHandlerAdapter;
import com.foxinmy.weixin4j.message.TextMessage;
import com.foxinmy.weixin4j.response.TextResponse;
import com.foxinmy.weixin4j.response.WeixinResponse;

import java.io.IOException;

/**
 * 文本消息处理
 *
 * @author jinyu(foxinmy @ gmail.com)
 * @className TextMessageHandler
 * @date 2015年11月18日
 * @since JDK 1.6
 */
@Component
public class TextMessageHandler extends MessageHandlerAdapter<TextMessage> {
    @Autowired
    private GetMessage getMessage;

    @Override
    public WeixinResponse doHandle0(TextMessage message) {
        String responseText = null;
        try {
            responseText = getMessage.getResponseText(message.getContent());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new TextResponse(responseText);
    }
}
