package com.dave.weixin.common.wechat.Configuration;

import com.foxinmy.weixin4j.cache.RedisCacheStorager;
import com.foxinmy.weixin4j.cache.RedisClusterCacheStorager;
import com.foxinmy.weixin4j.model.Token;
import com.foxinmy.weixin4j.mp.WeixinProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisCluster;

/**
 * Created by Dave on 2018/7/7
 * Describes
 */
@Configuration
public class WeiChatConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(WeiChatConfiguration.class);
    @Autowired
    JedisCluster jedisCluster;

    @Bean
    public WeixinProxy weixinProxy() {
        logger.info("微信配置信息初始化。。。。。。");
        //使用默认文件缓存
        //WeixinProxy weixinProxy = new WeixinProxy();

        //使用redis缓存
        return new WeixinProxy(new RedisClusterCacheStorager<Token>(jedisCluster));
    }


}
