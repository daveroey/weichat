package com.dave.weixin.common.wechat.Controller;

import com.foxinmy.weixin4j.exception.WeixinException;
import com.foxinmy.weixin4j.mp.WeixinProxy;
import com.foxinmy.weixin4j.mp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Dave on 2018/7/7
 * Describes
 */
@RestController
@RequestMapping(value = "/user/*")
public class UserController {
    @Autowired
    private WeixinProxy weixinProxy;

    @RequestMapping(value = "/getUser", method = RequestMethod.GET)
    public User getUser(@PathVariable String id) throws WeixinException {
        User user = weixinProxy.getUser(id);
        return user;
    }

}
