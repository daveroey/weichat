package com.dave.weixin.common.wechat.server.handler;

import java.util.Set;

import com.foxinmy.weixin4j.exception.WeixinException;
import com.foxinmy.weixin4j.handler.WeixinMessageHandler;
import com.foxinmy.weixin4j.mp.WeixinProxy;
import com.foxinmy.weixin4j.mp.model.User;
import com.foxinmy.weixin4j.request.WeixinMessage;
import com.foxinmy.weixin4j.request.WeixinRequest;
import com.foxinmy.weixin4j.response.TextResponse;
import com.foxinmy.weixin4j.response.WeixinResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 自定义处理消息
 *
 * @author jinyu(foxinmy @ gmail.com)
 * @className CustomMessageHandler
 * @date 2017年1月19日
 * @see
 * @since JDK 1.6
 */
@Component
public class CustomMessageHandler implements WeixinMessageHandler {
    private Logger log = LoggerFactory.getLogger(CustomMessageHandler.class);
    @Autowired
    private WeixinProxy weixinProxy;

    @Override
    public boolean canHandle(WeixinRequest request, WeixinMessage message, Set<String> nodeNames) {

        return message.getFromUserName().equals("xxx");

    }

    @Override
    public WeixinResponse doHandle(WeixinRequest request, WeixinMessage messager) {
        return new TextResponse("是你，是你，还是你。");
    }

    @Override
    public int weight() {
        return 0;
    }
}
