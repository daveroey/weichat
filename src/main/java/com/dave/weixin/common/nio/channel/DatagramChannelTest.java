package com.dave.weixin.common.nio.channel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * Created by Dave on 2018/7/12
 * Describes UDP传输协议通道
 */
public class DatagramChannelTest {
    ///////////////////////////////////////////////////////////////////////////
    // DatagramChannel是一个能收发UDP包的通道。
    // 因为UDP是无连接的网络协议，所以不能像其它通道那样读取和写入。它发送和接收的是数据包。
    ///////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) throws IOException {
        //打开UDP通道
        DatagramChannel channel = DatagramChannel.open();
        channel.socket().bind(new InetSocketAddress(9999));

        //接收数据
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.clear();
        //从DatagramChannel接受数据 如果Buffer容不下收到的数据，多出的数据将被丢弃。
        channel.receive(buffer);


        //发送数据
        String data = "光速QA";
        buffer.clear();
        buffer.put(data.getBytes());
        buffer.flip();
        //udp不会通知数据是否收到 传输是单向的
        int send = channel.send(buffer, new InetSocketAddress("www.baidu.com", 8080));


        // DatagramChannel连接到特定的地址
        // 由于UDP是无连接的，连接到特定地址并不会像TCP通道那样创建一个真正的连接。
        // 而是锁住DatagramChannel ，让其只能从特定地址收发数据。
        channel.connect(new InetSocketAddress("www.baidu.com", 8080));
        int read = channel.read(buffer);
        buffer.flip();
        int write = channel.write(buffer);


    }


}
