package com.dave.weixin.common.nio.channel;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by Dave on 2018/7/10
 * Describes Scatter 分散
 */
public class ScatterChannel {

    ///////////////////////////////////////////////////////////////////////////
    // 分散（scatter）从Channel中读取是指在读操作时将读取的数据写入多个buffer中。
    // 因此，Channel将从Channel中读取的数据“分散（scatter）”到多个Buffer中。
    ///////////////////////////////////////////////////////////////////////////


    public static void main(String[] args) throws IOException {

        RandomAccessFile raf = new RandomAccessFile("", "rw");
        FileChannel channel = raf.getChannel();

        ByteBuffer head = ByteBuffer.allocate(48);
        ByteBuffer body = ByteBuffer.allocate(1024);

        //当一个buffer写满后，紧接着写入下一个buffer。
        ByteBuffer[] byteArray = {head, body};
        long read = channel.read(byteArray);

    }


}