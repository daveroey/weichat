package com.dave.weixin.common.nio.selector;

import org.apache.tomcat.util.security.Escape;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Dave on 2018/7/11
 * Describes 通过选择器选择通道
 */
public class SelectorChannel {


    public static void main(String[] args) throws IOException {
        //选择通道
        Selector selector = Selector.open();
        //阻塞到至少有一个通道在你注册的事件上就绪了。返回值表明有一个或更多个通道就绪了
        int count = selector.select();
        //最长阻塞时间ms
        selector.select(1000 * 5);
        //不会阻塞，不管什么通道就绪都立刻返回 如果自从前一次选择操作后，没有通道变成可选择的，则此方法直接返回零
        selector.selectNow();


        //有通道就绪 可以通过selectedKeys()方法，访问“已选择键集（
        // selected key set）”中的就绪通道
        Set<SelectionKey> selectionKeys = selector.selectedKeys();

        //遍历这个已选择的键集合来访问就绪的通道。
        Iterator<SelectionKey> iterator = selectionKeys.iterator();
        while (iterator.hasNext()) {
            SelectionKey selectionKey = iterator.next();
            if (selectionKey.isAcceptable()) {
                //转型为自己需要处理的通道类型
                SocketChannel channel = (SocketChannel) selectionKey.channel();

            } else if (selectionKey.isConnectable()) {

            } else if (selectionKey.isReadable()) {

            } else if (selectionKey.isWritable()) {

            }
            //selectionKeys处理完不会自动移除SelectKey实例 手动干掉他丫的
            iterator.remove();
        }

        // 用完Selector后调用其close()方法会关闭该Selector，
        // 且使注册到该Selector上的所有SelectionKey实例无效。通道本身并不会关闭。
        selector.close();


    }
}
