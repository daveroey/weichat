package com.dave.weixin.common.nio.buffer;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.ScatteringByteChannel;

/**
 * Created by Dave on 2018/7/10
 * Describes buffer
 */
public class BufferTest {
    ///////////////////////////////////////////////////////////////////////////
    // 使用Buffer读写数据一般遵循以下四个步骤：
    //
    //写入数据到Buffer
    //调用flip()方法
    //从Buffer中读取数据
    //调用clear()方法或者compact()方法
    ///////////////////////////////////////////////////////////////////////////

    public static void main(String[] args) {
        //缓冲区本质上是一块可以写入数据，然后可以从中读取数据的内存
        //flip()方法:读和写的模式切换
        //clear()：清空整个缓冲区
        //compact():清空已经读取过的区域任何未读的数据都被移到缓冲区的起始处，新写入的数据将放到缓冲区未读数据的后面。

        //buffer 的三个属性
        //一、capacity 容量大小
        //二、position
        //三、limit

        //Buffer分配大小
        ByteBuffer buffer = ByteBuffer.allocate(48);

        //向buffer中写数据 也可以通过通道读取
        buffer.put("1".getBytes());


        //从buffer中读取数据 Buffer.rewind()将position设回0，所以你可以重读Buffer中的所有数据。limit保持不变，仍然表示能从Buffer中读取多少个元素（byte、char等）。
        Buffer rewind = buffer.rewind();

        //通过调用Buffer.mark()方法，可以标记Buffer中的一个特定position。之后可以通过调用Buffer.reset()方法恢复到这个position
        buffer.mark();
        buffer.reset();


    }
}
