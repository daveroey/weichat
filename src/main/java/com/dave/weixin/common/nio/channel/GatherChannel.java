package com.dave.weixin.common.nio.channel;

/**
 * Created by Dave on 2018/7/10
 * Describes gather聚合
 */
public class GatherChannel {
    ///////////////////////////////////////////////////////////////////////////
    // 聚集（gather）写入Channel是指在写操作时将多个buffer的数据写入同一个Channel，
    // 因此，Channel 将多个Buffer中的数据“聚集（gather）”后发送到Channel。
    // 分散于集合
    // gather经常用于需要将传输的数据分开处理的场合，例如传输一个由消息头和消息体组成的消息
    ///////////////////////////////////////////////////////////////////////////
}
