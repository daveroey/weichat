package com.dave.weixin.common.nio.pipe;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

/**
 * Created by Dave on 2018/7/12
 * Describes 管道
 */
public class PipeTest {

    ///////////////////////////////////////////////////////////////////////////
    // Java NIO管道是2个线程之间的单向数据连接。
    // Pipe有一个源通道和一个sink通道。
    // 数据会被写到sink通道，从源通道读取。
    ///////////////////////////////////////////////////////////////////////////


    public static void main(String[] args) throws IOException {
        //打开管道
        Pipe pipe = Pipe.open();

        //向管道写入数据 需要访问水槽通道像
        Pipe.SinkChannel sink = pipe.sink();

        String data = "write to pipe ..";
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.clear();
        buffer.put(data.getBytes());

        buffer.flip();
        while (buffer.hasRemaining()) {
            sink.write(buffer);
        }

        //从管道读取数据
        //获取源通道
        Pipe.SourceChannel source = pipe.source();
        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        int read = source.read(readBuffer);


    }
}
