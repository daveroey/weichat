package com.dave.weixin.common.nio.path;

import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by Dave on 2018/7/12
 * Describes
 */
public class PathTest {

    ///////////////////////////////////////////////////////////////////////////
    // Path实例表示文件系统中的路径。路径可以指向文件或目录。
    // 路径可以是绝对的或相对的。绝对路径包含从文件系统根目录到其指向的文件或目录的完整路径。
    // 相对路径包含相对于其他路径的文件或目录的路径。
    // java.nio.file.Path界面类似于java.io.File 类，但有一些细微的差别。
    // 但在许多情况下，您可以File使用Path接口替换类的使用。
    ///////////////////////////////////////////////////////////////////////////


    public static void main(String[] args) throws IOException {

        //创建实例d
        Path path = Paths.get("/Users/mac/Downloads/file.txt");

        //Files 判断path是否存在
        boolean exists = Files.exists(path);
        Assert.isTrue(exists, "不存在");

        //创建新的目录
        Files.createDirectory(path);

        //文件复制
        Path target = Paths.get("/users/mac/gg");
        Files.copy(path, target);

        //重命名
        Path sourcePath = Paths.get("data / logging-copy.properties");
        Files.move(sourcePath, target, StandardCopyOption.REPLACE_EXISTING);

        //删除文件或者目录
        Files.delete(path);


        try {
            //递归遍历目录
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            });
        } catch (Exception e) {

        }


    }
}
