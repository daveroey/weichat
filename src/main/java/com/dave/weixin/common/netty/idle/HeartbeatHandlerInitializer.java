package com.dave.weixin.common.netty.idle;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;


/**
 * @Author Dave
 * @Date 2018/7/17
 * @Description
 */
public class HeartbeatHandlerInitializer extends ChannelInitializer<Channel> {

    /**
     * 读超时
     */
    private static final int READ_IDEL_TIME_OUT = 4;

    /**
     * 写超时
     */
    private static final int WRITE_IDEL_TIME_OUT = 5;

    //所有超时
    private static final int ALL_IDEL_TIME_OUT = 7;


    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        pipeline.addLast(new IdleStateHandler(READ_IDEL_TIME_OUT,
                WRITE_IDEL_TIME_OUT, ALL_IDEL_TIME_OUT, TimeUnit.SECONDS));
        pipeline.addLast(new HearbeatServerHandler()); // 2

    }
}
