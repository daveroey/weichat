package com.dave.weixin.common.netty.webchat.listener;

import com.dave.weixin.common.netty.webchat.WebSocketChatServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @Author Dave
 * @Date 2018/7/17
 * @Description WebSocket启动线程
 */
@WebListener
public class WebSocketStartListener implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(WebSocketStartListener.class);
    private final int port;

    public WebSocketStartListener() {
        this.port = 9999;
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Thread thread = new Thread(() -> {
            try {
                logger.debug("启动成功");
                logger.info("启动成功");
                logger.trace("启动成功");
                //启动WebSocket
                new WebSocketChatServer(port).run();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.setDaemon(true);
        thread.start();

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
